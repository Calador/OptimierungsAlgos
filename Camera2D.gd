extends Camera2D

const SPEED = 800
const ZOOM_FACTOR = 2
const MIN_ZOOM = 1
const MAX_ZOOM = 50
var move_dir = Vector2()
var zoom_change: float


func get_input():
	move_dir = Input.get_vector("left", "right", "up", "down")
	
	#zoom_change = Input.get_axis("zoom_out", "zoom_in")
	zoom_change = 0
	if Input.get_action_strength("zoom_in") > 0:
		zoom_change = 0.25
	if Input.get_action_strength("zoom_out") > 0:
		zoom_change = 2

func _physics_process(delta):
	get_input()
	if move_dir != Vector2.ZERO:
		position += move_dir * SPEED * delta * 1/zoom.x
	if zoom_change != 0:
		change_zoom(delta)

func change_zoom(delta: float) -> void:
	var mouse_pos := get_global_mouse_position()
	var zoom_value = zoom.x * (1 - delta) + (zoom.x * zoom_change) * delta
	zoom_value = clamp(zoom_value, MIN_ZOOM, MAX_ZOOM)
	
	zoom = Vector2(zoom_value, zoom_value)
