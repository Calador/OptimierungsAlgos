class_name Greedy
extends Object

func find_solution(instance: Instance, selection: Selection):
	selection.set_instance(instance)
	var insert = InsertGreedy.new()
	insert.reset(instance)
	while true:
		var next_rect = selection.get_next()
		if !next_rect:
			break
		insert.insert_rect(next_rect)
		await GO.main.do_next_step()

