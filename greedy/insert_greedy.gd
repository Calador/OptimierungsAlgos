class_name InsertGreedy
extends Node

var instance: Instance
var positionings: Array[Positioning] = []

func reset(_instance: Instance):
	GO.main.draw_gui.ins_gred = self
	instance = _instance

func insert_rect(rect: Rectangle):
	for i in instance.__boxes.size():
		if i >= positionings.size():
			positionings.append(Positioning.new(instance.__boxes[i]))
		var pos = positionings[i]
		if await pos.place_rectangle(rect):
			return
	instance.add_box()
	insert_rect(rect)
