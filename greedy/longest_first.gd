class_name LongestFirst
extends Selection

func set_instance(instance: Instance):
	rects = instance.rects.duplicate()
	for rect in rects:
		if rect.size.x < rect.size.y:
			rect.rotate()
	rects.sort_custom(func(a, b):
		if a.size.x > b.size.x:
			return true
		return a.size.y > b.size.y)
	
