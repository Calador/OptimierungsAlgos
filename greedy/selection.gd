class_name Selection
extends Object

var rects: Array[Rectangle] = []

func set_instance(instance: Instance):
	rects = instance.rects.duplicate()

func get_next() -> Rectangle:
	if rects.size() == 0:
		return null
	var next = rects[0]
	rects.remove_at(0)
	return next
