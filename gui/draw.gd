class_name Draw
extends Node2D

const MAX_BOXES = 6

var instance: Instance
var last_rect: Rectangle
var ins_gred: InsertGreedy
var posi: Positioning
var posi_box: Box

func _draw():
	if !instance:
		return
	for i in instance.__boxes.size():
		var box = instance.__boxes[i]
		var length = box.length
		box.position = Vector2i((i % MAX_BOXES) * (length + 1), int(i / MAX_BOXES)* (length + 1))
		
		draw_rect(Rect2(box.position.x - 0.5, box.position.y - 0.5, length + 1, length + 1), Color.BLACK, false, 1.0)
		# box background
		for x in box.length:
			for y in box.length:
				var color = Color(0.1, 0.1, 0.1)
				if (x+y) % 2 == 0:
					color = Color(0.2, 0.2, 0.2)
				draw_rect(Rect2(box.position.x + x, box.position.y + y, 1, 1), color)
		for rect in box.rectangles:
			var color = rect.color
			if rect == last_rect:
				color = Color.WHITE
			draw_rect(Rect2(box.position.x + rect.position.x, box.position.y + rect.position.y, rect.size.x, rect.size.y), color)
	
	if posi:
		draw_positioning(posi, posi_box)
	
	if ins_gred:
		print(ins_gred)
		for i in ins_gred.positionings.size():
			if ins_gred.positionings[i] == null:
				continue
			draw_positioning(ins_gred.positionings[i], instance.__boxes[i])

func draw_positioning(positionings, box):
	var base_pos = Vector2i(box.position)
	var box_length = box.length
	for line in positionings.lines:
		if line.position.y >= box_length:
			continue
		draw_line(base_pos + line.position, base_pos + line.position + Vector2i(line.length, 0), Color.RED, 0.2)

func reset():
	instance = null
	last_rect = null
	ins_gred = null
	posi = null
	posi_box = null
