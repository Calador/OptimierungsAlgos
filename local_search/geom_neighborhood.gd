class_name GeomNeighborhood
extends Neighborhood

var rects: Array[Rectangle]
var box: Box = null
var positioning: Positioning

func find_start_instance(_instance: Instance):
	instance = _instance
	rects = instance.rects.duplicate()
	for rect in rects:
		if rect.size.x < rect.size.y:
			rect.rotate()
	rects.sort_custom(func(a, b):
		if a.size.x * a.size.y > b.size.x * b.size.y:
			return true
		return a.size.y > b.size.y)
	for rect in rects:
		var box = instance.add_box()
		box.add_rect(rect)
	box = instance.__boxes[0]
	positioning = Positioning.new(box)
	GO.main.draw_gui.posi = positioning
	GO.main.draw_gui.posi_box = box

func next_instance() -> bool:
	if rects.size() == 0:
		# Clear empty boxes
		var i = instance.__boxes.size() - 1 - instance.__boxes.find(box)
		for o in i:
			instance.__boxes.remove_at(instance.__boxes.size() - 1)
		return false
	for i in rects.size():
		var rect = rects[i]
		if await positioning.place_rectangle(rect):
			rects.remove_at(i)
			return true
	# No rect fits anymore in this box
	var i = instance.__boxes.find(box)
	box = instance.__boxes[i + 1]
	positioning.reset(box)
	GO.main.draw_gui.posi_box = box
	return await next_instance()
