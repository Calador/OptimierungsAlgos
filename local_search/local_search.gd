class_name LocalSearch
extends Object

func find_solution(instance: Instance, neighborhood: Neighborhood):
	# find valid solution
	neighborhood.find_start_instance(instance)
	await GO.main.do_next_step()
	while true:
		if !await neighborhood.next_instance():
			return
		await GO.main.do_next_step()
