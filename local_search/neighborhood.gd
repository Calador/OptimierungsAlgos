class_name Neighborhood
extends Node

var instance: Instance

func find_start_instance(_instance: Instance):
	instance = _instance

func next_instance() -> bool:
	return false
