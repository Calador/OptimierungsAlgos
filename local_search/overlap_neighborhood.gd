class_name OverlapNeighborhood
extends Neighborhood

const OVERLAP_STEPS = 0.4

var box: Box = null
var positioning: Positioning
var rects_left: Array[Rectangle] = []
var new_rects_left: Array[Rectangle] = []
var place_rects: Array[Rectangle] = []
var overlap = 1

func find_start_instance(_instance: Instance):
	instance = _instance
	var rects = instance.rects.duplicate()
	for rect in rects:
		if rect.size.x < rect.size.y:
			rect.rotate()
	rects.sort_custom(func(a, b):
		if a.size.x * a.size.y > b.size.x * b.size.y:
			return true
		return a.size.y > b.size.y)
	
	box = instance.add_box()
	for rect in rects:
		box.add_rect(rect)
	
	positioning = Positioning.new(box)
	GO.main.draw_gui.posi = positioning
	GO.main.draw_gui.posi_box = box
	box = null

func next_instance() -> bool:
	if box == null:
		if overlap > 0:
			overlap = max(overlap - OVERLAP_STEPS, 0)
			positioning.overlap = overlap
			box = instance.__boxes[0]
			positioning.reset(box)
			GO.main.draw_gui.posi_box = box
			place_rects = box.rectangles
			box.rectangles = []
			return await next_instance()
		return false
	while true:
		if place_rects.size() == 0:
			while true:
				if rects_left.size() == 0:
					break
				var rect = rects_left[0]
				rects_left.remove_at(0)
				if await positioning.place_rectangle(rect):
					return true
				new_rects_left.append(rect)
			break
		var rect = place_rects[0]
		place_rects.remove_at(0)
		
		if await positioning.place_rectangle(rect):
			#box.add_rect(rect)
			return true
		
		new_rects_left.append(rect)
	rects_left = new_rects_left
	new_rects_left = []
	
	var i = instance.__boxes.find(box)
	if i + 1 < instance.__boxes.size():
		box = instance.__boxes[i + 1]
		positioning.reset(box)
		GO.main.draw_gui.posi_box = box
		place_rects = box.rectangles
		box.rectangles = []
		return await next_instance()
	if rects_left.size() > 0:
		box = instance.add_box()
		positioning.reset(box)
		place_rects = rects_left
		rects_left = []
		GO.main.draw_gui.posi_box = box
		return await next_instance()
	box = null
	return await next_instance()


