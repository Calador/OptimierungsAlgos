class_name RuleNeighborhood
extends Neighborhood

var rects: Array[Rectangle]
var box: Box = null
var positioning: Positioning
# index of the current spot in permutation
var current_index: int = 0

func find_start_instance(_instance: Instance):
	instance = _instance
	rects = instance.rects.duplicate()
	for rect in rects:
		if rect.size.x < rect.size.y:
			rect.rotate()
	rects.sort_custom(func(a, b):
		if a.size.x * a.size.y > b.size.x * b.size.y:
			return true
		return a.size.y > b.size.y)
	for rect in rects:
		var box = instance.add_box()
		box.add_rect(rect)
	box = instance.__boxes[0]
	positioning = Positioning.new(box)
	GO.main.draw_gui.posi = positioning
	GO.main.draw_gui.posi_box = box

# It would be more "true" to rule based to re insert all rect at every stepp, but this is not necesary
func next_instance() -> bool:
	if current_index >= rects.size():
		# Clear empty boxes
		var i = instance.__boxes.size() - 1 - instance.__boxes.find(box)
		for o in i:
			instance.__boxes.remove_at(instance.__boxes.size() - 1)
		return false
	for i in range(current_index, rects.size()):
		var rect = rects[i]
		if await positioning.place_rectangle(rect):
			rects.remove_at(i)
			rects.insert(current_index,rect)
			current_index += 1
			return true
	# No rect fits anymore in this box
	var i = instance.__boxes.find(box)
	box = instance.__boxes[i + 1]
	positioning.reset(box)
	GO.main.draw_gui.posi_box = box
	return await next_instance()
