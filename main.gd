class_name Main
extends Node2D

signal next_step()

@onready var draw_gui: Draw = $Draw
@export var should_draw: bool = true

var search: Object
var problem: Problem
var wait_with_steps: bool = true

func _init():
	GO.main = self

func _ready():
	_restart()

func _restart():
	if search:
		search.free()
	draw_gui.reset()
	var cb: CheckButton = $CanvasLayer/VBoxContainer/Label4/NewProblem
	if !problem || $CanvasLayer/VBoxContainer/Label4/NewProblem.button_pressed:
		var box_length = $CanvasLayer/VBoxContainer/BoxLength.value
		var amount_rects = $CanvasLayer/VBoxContainer/RectAm.value
		var min_w = $"CanvasLayer/VBoxContainer/HBoxContainer/MinWidth".value
		var max_w = $CanvasLayer/VBoxContainer/HBoxContainer/MaxWidth.value
		var min_h = $CanvasLayer/VBoxContainer/HBoxContainer2/MinHeight.value
		var max_h = $CanvasLayer/VBoxContainer/HBoxContainer2/MaxHeight.value
		problem = Problem.new(
				box_length, 
				amount_rects, 
				Vector2i(min_w, max_w), 
				Vector2i(min_h, max_h)
				)
	var instance = Instance.new(problem)
	draw_gui.instance = instance
	var greedy = Greedy.new()
	var local_search = LocalSearch.new()
	var selected = $CanvasLayer/VBoxContainer/Algo.selected
	match selected:
		0:
			search = local_search
			local_search.find_solution(instance, GeomNeighborhood.new())
		1:
			search = local_search
			local_search.find_solution(instance, RuleNeighborhood.new())
		2:
			search = local_search
			local_search.find_solution(instance, OverlapNeighborhood.new())
		3:
			search = greedy
			greedy.find_solution(instance, LongestFirst.new())
		4:
			search = greedy
			greedy.find_solution(instance, RandomFirst.new())
	#var selection = RandomFirst.new()
	#var selection = LongestFirst.new()
	#var neighboor = OverlapNeighborhood.new()
	#
	##greedy.find_solution(instance, selection)
	#local_search.find_solution(instance, neighboor)

func _unhandled_input(event):
	if event.is_action_pressed("do_next_step"):
		next_step.emit()

func do_next_step(): 
	if should_draw:
		draw_gui.queue_redraw()
	if wait_with_steps:
		await next_step



func _benchmark():
	if search:
		search.free()
		search = null
	draw_gui.reset()
	var cb: CheckButton = $CanvasLayer/VBoxContainer/Label4/NewProblem
	var box_length = $CanvasLayer/VBoxContainer/BoxLength.value
	var amount_rects = $CanvasLayer/VBoxContainer/RectAm.value
	var min_w = $"CanvasLayer/VBoxContainer/HBoxContainer/MinWidth".value
	var max_w = $CanvasLayer/VBoxContainer/HBoxContainer/MaxWidth.value
	var min_h = $CanvasLayer/VBoxContainer/HBoxContainer2/MinHeight.value
	var max_h = $CanvasLayer/VBoxContainer/HBoxContainer2/MaxHeight.value
	var instances = $CanvasLayer/VBoxContainer/Instances.value
	var t_geo = 0
	var b_goe = 0
	var t_rule = 0
	var b_rule = 0
	var t_over = 0
	var b_over = 0
	var t_long = 0
	var b_long = 0
	var t_rand = 0
	var b_rand = 0
	
	var greedy = Greedy.new()
	var local_search = LocalSearch.new()
	wait_with_steps = false
	for i in instances:
		var test_problem = Problem.new(
				box_length, 
				amount_rects, 
				Vector2i(min_w, max_w), 
				Vector2i(min_h, max_h)
				)
		
		var instance
		var time_s
		
		instance = Instance.new(test_problem)
		time_s = Time.get_unix_time_from_system()
		await local_search.find_solution(instance, GeomNeighborhood.new())
		t_geo += Time.get_unix_time_from_system() - time_s
		b_goe += instance.__boxes.size()
		
		instance = Instance.new(test_problem)
		time_s = Time.get_unix_time_from_system()
		await local_search.find_solution(instance, RuleNeighborhood.new())
		t_rule += Time.get_unix_time_from_system() - time_s
		b_rule += instance.__boxes.size()
		
		instance = Instance.new(test_problem)
		time_s = Time.get_unix_time_from_system()
		await local_search.find_solution(instance, OverlapNeighborhood.new())
		t_over += Time.get_unix_time_from_system() - time_s
		b_over += instance.__boxes.size()
		
		instance = Instance.new(test_problem)
		time_s = Time.get_unix_time_from_system()
		await greedy.find_solution(instance, LongestFirst.new())
		t_long += Time.get_unix_time_from_system() - time_s
		b_long += instance.__boxes.size()
		
		instance = Instance.new(test_problem)
		time_s = Time.get_unix_time_from_system()
		await greedy.find_solution(instance, RandomFirst.new())
		t_rand += Time.get_unix_time_from_system() - time_s
		b_rand += instance.__boxes.size()
	
	print("")
	print("--- Test Result ---")
	print(str(instances) + " runs on " + str(amount_rects) + " rectangles")
	_print_algo_res("Geo:     ", t_geo, b_goe, instances)
	_print_algo_res("Rule:    ", t_rule, b_rule, instances)
	_print_algo_res("Over:    ", t_over, b_over, instances)
	_print_algo_res("Longest: ", t_long, b_long, instances)
	_print_algo_res("Rand:    ", t_rand, b_rand, instances)
	wait_with_steps = true

func _print_algo_res(algo: String, t, b, instances):
	print(algo + "Avg. Time: " + str(t / instances) + " Avg. Boxes: " + str(b / instances))
