class_name Positioning
extends Object

class Line:
	var position: Vector2i
	var length: int
	
	func _init(pos: Vector2i, leng: int):
		position = pos
		length = leng
	func _to_string():
		return "Line: Pos: " + str(position) + ", length: " + str(length)

var box_size: int = 0
var lines: Array[Line]
var box: Box
var overlap: float = 0

func _init(_box: Box):
	box = _box
	box_size = box.length
	lines = []
	lines.append(Line.new(Vector2i(0,0), box.length))

func reset(box: Box):
	_init(box)

func place_rectangle(rect: Rectangle, add_rect: bool = true) -> bool:
	if rect.size.x < rect.size.y:
		rect.rotate()
	var min_y = box_size
	var index_of_min = -1
	var align_left: bool
	var rotated: bool
	for i in lines.size():
		var line = lines[i]
		if line.position.y >= min_y:
			continue
		
		var result = __rectangle_fits(rect, i, false)
		if result != null:
			min_y = line.position.y
			index_of_min = i
			align_left = result
			rotated  = false
		else:
			var result_rotated = __rectangle_fits(rect, i, true)
			if result_rotated != null:
				min_y = line.position.y
				index_of_min = i
				align_left = result_rotated
				rotated  = true
	var line = lines[index_of_min]
	if index_of_min < 0:
		return false
	if rotated:
		rect.rotate()
	if align_left:
		rect.position = line.position
	else:
		rect.position = line.position + Vector2i(line.length - rect.size.x, 0)
	if add_rect:
		box.add_rect(rect)
	GO.main.draw_gui.last_rect = rect
	
	__update_lines(Line.new(rect.position + Vector2i(0, ceil(rect.size.y * (1 - overlap))), rect.size.x), index_of_min, align_left)
	await __validate_lines()
	return true

func __rectangle_fits(rect: Rectangle, line_num: int, rotated: bool):
	var line = lines[line_num]
	var rect_hight: int = rect.size.x if rotated else rect.size.y
	if line.position.y + rect_hight > box_size:
		return null
	var left_waste = __rectangle_fits_side(rect, line_num, true, rotated)
	var right_waste = __rectangle_fits_side(rect, line_num, false, rotated)
	if right_waste < left_waste && right_waste < box_size * box_size:
		return false
	elif left_waste < box_size * box_size:
		return true


func __rectangle_fits_side(rect: Rectangle, line_num: int, left: bool, rotated: bool) -> int:
	var size = rect.size
	if rotated:
		var temp_x = size.x
		size.x = size.y
		size.y = temp_x
	
	var start_line_hight = lines[line_num].position.y
	var length_spare = size.x
	var waste = 0
	
	# Check if rect will be in box
	if left:
		if lines[line_num].position.x + length_spare > box_size:
			return box_size * box_size
	else:
		if lines[line_num].position.x + lines[line_num].length  - length_spare < 0:
			return box_size * box_size
	
	while length_spare > 0:
		if line_num < 0 || line_num >= lines.size():
			return box_size * box_size
		
		var line = lines[line_num]
		
		# <= start hight
		if start_line_hight < line.position.y:
			return box_size * box_size
		
		if length_spare > line.length:
			waste += (start_line_hight - line.position.y) * line.length
		else:
			waste += (start_line_hight - line.position.y) * length_spare
		
		length_spare -= line.length
		
		line_num += 1 if left else -1
	return waste

func __update_lines(new_line: Line, line_num: int, left: bool):
	if left:
		lines.insert(line_num, new_line)
		line_num += 1
	else:
		lines.insert(line_num+1, new_line)
	var length_spare = new_line.length
	while length_spare > 0:
		if line_num < 0 || line_num >= lines.size():
			return
		
		var line = lines[line_num]
		
		if length_spare >= line.length:
			length_spare -= line.length
			lines.remove_at(line_num)
		else:
			if left:
				lines[line_num].position.x += length_spare
			lines[line_num].length -= length_spare
			return
		if !left:
			# only change index if move to right, otherwise changed by delete
			line_num -= 1

func _pr_lines():
	var string = ""
	for line in lines: 
		string += str(line.position) + ": " + str(line.length) + ", "
	print(string)

func __validate_lines():
	var x_pos: int = 0
	for line in lines:
		if line.position.x != x_pos:
			printerr(line._to_string() + " expected to start at pos " + str(x_pos))
			await GO.main.do_next_step()
		x_pos += line.length
	if x_pos != box_size:
		printerr("Total lin length " + str(box_size) + " expected, but is" + str(x_pos))
