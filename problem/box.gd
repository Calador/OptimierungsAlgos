class_name Box
extends Object

var position: Vector2
var length: int
var rectangles: Array[Rectangle] = []
var instance: Instance

func add_rect(rect: Rectangle):
	if rect.in_box:
		rect.in_box.remove_rect(rect)
	rect.in_box = self
	rectangles.append(rect)

func remove_rect(rect: Rectangle):
	rectangles.erase(rect)
