class_name Instance
extends Object


var box_length: int = 0
var __boxes: Array[Box] = []
var rects: Array[Rectangle]

func _init(problem: Problem):
	box_length = problem.box_size
	rects = problem.rects.duplicate()

func add_box() -> Box:
	var box = Box.new()
	box.length = box_length
	box.instance = self
	__boxes.append(box)
	return box
