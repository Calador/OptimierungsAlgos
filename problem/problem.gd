class_name Problem
extends Object

var rects: Array[Rectangle] = []
var box_size: int

func _init(_box_size: int, _num_rect: int, _height_range: Vector2i, _width_range: Vector2i):
	box_size = _box_size
	for i in _num_rect:
		
		var width = randf_range(_width_range.x, _width_range.y)
		var height = randf_range(_height_range.x, _height_range.y)
		var rect = Rectangle.new(Vector2i(width, height))
		rects.append(rect)
	
