class_name Rectangle
extends Object

var size: Vector2i
var position: Vector2i
var color: Color
var in_box: Box

func _init(_size: Vector2i):
	size = _size
	color = rand_color()

func rotate():
	var temp = size.x 
	size.x = size.y
	size.y = temp

static func rand_color() -> Color:
	var r = randf()
	var g = randf()
	var b = randf()
	var leng = r+g+b
	return Color(r/leng, g/leng, b/leng)
